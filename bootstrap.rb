# frozen_string_literal: true

require_relative 'lib/entrada'
require_relative 'lib/sonda'

caminho_arquivo = ARGV[0]

entrada = Entrada.new(caminho_arquivo)

entrada.sondas.each do |sonda|
  sonda = Sonda.new(sonda)
  sonda.executar_movimentos
  puts sonda.posicao_atual
end
