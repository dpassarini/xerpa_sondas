class Movimento
  attr_reader :movimento, :x, :y, :direcao, :x_limite, :y_limite
  DIRECOES = %w[N E S W].freeze

  def initialize(coordenada_inicio, movimento, limite)
    @movimento = movimento
    @direcao = coordenada_inicio[2]
    @x = coordenada_inicio[0].to_i
    @y = coordenada_inicio[1].to_i
    @x_limite = limite[0].to_i
    @y_limite = limite[1].to_i
  end

  def movimentar
    if movimento == 'M'
      mudar_coordenada
    else
      mudar_direcao
    end
  end

  private
  
  def mudar_coordenada
    case direcao
    when 'E'
      @x = @x < @x_limite ? @x + 1 : @x_limite
    when 'W'
      @x = @x > 0 ? @x - 1 : 0
    when 'N'
      @y = @y < @y_limite ? @y + 1 : @y_limite
    when 'S'
      @y = @y > 0 ? @y - 1 : 0
    end
    [x, y, direcao]
  end

  def mudar_direcao
    idx = DIRECOES.find_index(direcao)
    idx += 1 if movimento == 'R'
    idx += 3 if movimento == 'L'
    [x, y, DIRECOES[idx % 4]]
  end
end
