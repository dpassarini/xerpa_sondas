class Entrada
  attr_accessor :limite, :sondas

  def initialize(caminho_arquivo)
    begin
      entrada = File.read(caminho_arquivo).split("\n")
      @limite = entrada.shift.split(' ')
      @sondas = []
      definir_sondas(entrada, limite)
    rescue TypeError
      puts "Arquivo invalido ou inexistente"
      exit!
    end  
  end

  private

  def definir_sondas(entrada, limite)
    sonda = {}
    sonda[:posicao_inicial] = entrada.shift.split(' ')
    sonda[:movimentos] = entrada.shift.split(' ')
    sonda[:limite] = limite
    @sondas << sonda
    definir_sondas(entrada, limite) unless entrada.empty?
  end
end
