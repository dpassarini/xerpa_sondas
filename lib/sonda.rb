require_relative 'movimento'

class Sonda
  attr_reader :movimentos, :posicao, :limite

  def initialize(instrucoes)
    @movimentos = instrucoes[:movimentos]
    @posicao = instrucoes[:posicao_inicial]
    @limite = instrucoes[:limite]
  end

  def executar_movimentos
    @movimentos.each do |movimento|
      @posicao = Movimento.new(@posicao, movimento, limite).movimentar
    end
  end

  def posicao_atual
    @posicao.join(' ')
  end
end
