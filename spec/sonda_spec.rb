require 'spec_helper'
require_relative '../lib/sonda'

describe Sonda do
  let(:movimentos) { ['L'] }
  let(:posicao) { ['E', '5', '4'] }
  let(:limite) {[ '5', '6'] }
  let(:instrucoes){
      instrucoes = {
        movimentos: movimentos,
        posicao_inicial: posicao,
        limite: limite
      }
  }

  let(:sonda) { Sonda.new(instrucoes) }

  describe '#initialize' do
    it 'deve povoar as propiredades corratemante' do
      expect(sonda.movimentos).to eq(movimentos)
      expect(sonda.posicao).to eq(posicao)
      expect(sonda.limite).to eq(limite)
    end
  end

  describe '#executar_movimentos' do
    it 'deve chamar os metodos de movimento' do
      allow(Movimento).to receive(:new).with(posicao, movimentos.first, limite).and_return(OpenStruct.new(movimentar: true))
      expect(Movimento).to receive(:new).with(posicao, movimentos.first, limite)
      sonda.executar_movimentos
    end
  end

  describe "#posicao_atual" do
    it 'deve retornar a posicao formatada' do
      expect(sonda.posicao_atual).to eq("E 5 4")
    end
  end
  
end
