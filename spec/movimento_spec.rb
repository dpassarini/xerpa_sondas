require 'spec_helper'
require_relative '../lib/movimento'

describe Movimento do
  let(:virar_esquerda) { 'L' }
  let(:virar_direita) { 'R' }
  let(:mover) { 'M' }
  let(:coordenada_inicio) { ['2', '3', 'E'] }
  let(:limite) {[ '5', '6'] }
  let(:y_limite) { 6 }
  let(:x_limite) { 5 }
  let(:x) { 2 }
  let(:y) { 3 }
  let(:direcao_leste) { 'E' }
  

  let(:movimento_adiante) { Movimento.new(coordenada_inicio, mover, limite) }
  let(:movimento_direito) { Movimento.new(coordenada_inicio, virar_direita, limite) }
  let(:movimento_esquerdo) { Movimento.new(coordenada_inicio, virar_esquerda, limite) }

  describe '#initialize' do
    it 'deve povoar as propiredades corratemante' do
      expect(movimento_adiante.x_limite).to eq(x_limite)
      expect(movimento_adiante.y_limite).to eq(y_limite)
      expect(movimento_adiante.x).to eq(x)
      expect(movimento_adiante.y).to eq(y)
      expect(movimento_adiante.direcao).to eq(direcao_leste)
      expect(movimento_adiante.movimento).to eq(mover)
    end
  end

  describe "#movimentar" do
    context 'A sonda esta virada para leste' do
      it 'e ao virar para esquerda, deve terminar virada para o norte' do
        resultado = movimento_esquerdo.movimentar
        expect(resultado).to eq([x, y, 'N'])  
      end

      it 'e ao virar para direita, deve terminar virada para o sul' do
        resultado = movimento_direito.movimentar
        expect(resultado).to eq([x, y, 'S'])  
      end

      it 'e irá em frente, logo deve andar um x para a direita (x+1) ainda a leste' do
        resultado = movimento_adiante.movimentar
        expect(resultado).to eq([x+1, y, 'E'])  
      end
    end

    context 'A sonda está no limite leste' do
      it 'ao tentar andar para a direita, não mudará a coordenada' do
        mov_limite_direito = Movimento.new([5, 0, 'E'], mover, limite)
        resultado = mov_limite_direito.movimentar
        expect(resultado).to eq([5, 0, 'E'])
      end
    end

    context 'A sonda está no limite sul' do
      it 'ao tentar andar para o sul, não mudará a coordenada' do
        mov_limite_direito = Movimento.new([0, 0, 'S'], mover, limite)
        resultado = mov_limite_direito.movimentar
        expect(resultado).to eq([0, 0, 'S'])
      end
    end

    context 'A sonda está no limite norte' do
      it 'ao tentar andar para o norte, não mudará a coordenada' do
        mov_limite_direito = Movimento.new([5, 6, 'N'], mover, limite)
        resultado = mov_limite_direito.movimentar
        expect(resultado).to eq([5, 6, 'N'])
      end
    end

    context 'A sonda está no limite oeste' do
      it 'ao tentar andar para o norte, não mudará a coordenada' do
        mov_limite_direito = Movimento.new([0, 6, 'W'], mover, limite)
        resultado = mov_limite_direito.movimentar
        expect(resultado).to eq([0, 6, 'W'])
      end
    end
  end
  
end
