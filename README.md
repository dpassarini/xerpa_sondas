# Desafio XERPA - Sondas

##Instruções para rodar o desafio

### Versão da linguagem Ruby
Esse software foi desenvolvido em Ruby, na versão 2.6.3

### Obtendo o software
Clone esse repositório. Entre no diretório clonado:
```sh
$ cd xerpa_sondas
```
E baixe a dependência usando o comando bundle:
```sh
$ bundle
```
Após essa etapa, já é possível rodar os testes, usando o comando abaixo:
```sh
$ rspec
```

##Utilizando o software
O método de entrada escolhido é o arquivo. Na primeira linha deve ser dado o limite superior direito do retangulo, sendo que essas coordenadas devem ser **SEPARADAS POR ESPAÇOS**. Em caso de dúvida, utilizar o arquivo **entrada** contido nesse repositório como exemplo.

Após essa primeira linha, cria-se para duas linhas para cada sonda. A primeira linha por sonda será a coordenada inicial dela e sua posição em relação à rosa dos ventos. A segunda linha será a sequência de movimentos. Em caso de dúvida, utilizar o arquivo **entrada** contido nesse repositório como exemplo.

No diretório onde esse repositório foi clonado, rode o seguinte comando:
```sh
$ ruby bootstrap.rb /caminho/do/arquivo/entrada
```

Para usar o arquivo de exemplo, rode:
```sh
$ ruby bootstrap.rb entrada
```